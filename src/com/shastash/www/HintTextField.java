package com.shastash.www;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

class HintTextField extends JTextField implements FocusListener {
	
	private final String hint;
	private boolean showingHint;

	public HintTextField(final String hint){
		super(hint);
		this.hint = hint;
		this.setForeground(new Color(150, 150, 150));
		this.showingHint = true;
		super.addFocusListener(this);
	}

	public HintTextField(final String hint, int i){
		super(hint, i);
		this.hint = hint;
		this.setForeground(new Color(150, 150, 150));
		this.showingHint = true;
		super.addFocusListener(this);
	}

	public void focusGained(FocusEvent e){
		this.setForeground(new Color(0, 0, 0));
		if(this.getText().isEmpty()){
			super.setText("");
			showingHint = false;
		}
	}
	
	public void focusLost(FocusEvent e){
		if(this.getText().isEmpty()){
			super.setText(hint);
			showingHint = true;
			this.setForeground(new Color(150, 150, 150));
		}
		else
			this.setForeground(new Color(0, 0, 0));
	}

	@Override
	public String getText(){
		return showingHint ? "" : super.getText();
	}
}