package com.shastash.www;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AbilityDictionary {	
	HashMap<Integer, Ability> ability_dictionary;
	private final int GCD_LIST_INDEX = 0;
	private final int SATA_LIST_INDEX = 1;
	private final int KISS_LIST_INDEX = 2;
	private final int NINJUTSU_LIST_INDEX = 3;
	
	//================================================================================================== Shared cooldown lists
	HashMap<Integer, ArrayList<Integer>> cooldown_matrix;
	ArrayList<Integer> gcd_cd_list;
	ArrayList<Integer> sata_cd_list;
	ArrayList<Integer> kiss_cd_list;
	ArrayList<Integer> ninjutsu_cd_list;
	
	public AbilityDictionary(String job){
		ability_dictionary = new HashMap<Integer, Ability>();
		cooldown_matrix = new HashMap<Integer, ArrayList<Integer>>();
		gcd_cd_list = new ArrayList<Integer>();
		cooldown_matrix.put(GCD_LIST_INDEX, gcd_cd_list);
		
		/*
		if(job.equals("PLD")){
			//================================================================================================== PLD
			Ability fast_blade
			Ability rampart
			Ability savage_blade
			Ability fight_or_flight
			Ability flash
			Ability convalescence
			Ability riot_blade
			Ability shield_lob
			Ability shield_bash
			Ability provoke
			Ability rage_of_halone
			Ability shield_swipe
			Ability awareness
			Ability sentinel
			Ability tempered_will
			Ability bulwark
			Ability circle_of_scorn
			Ability sword_oath
			Ability cover
			Ability shield_oath
			Ability spirits_within
			Ability hallowed_ground	
		}
		elseif(job.equals("MNK"){
			//================================================================================================== MNK
			Ability bootshine
			Ability true_strike
			Ability featherfoot
			Ability snap_punch
			Ability second_wind
			Ability haymaker
			Ability internal_release
			Ability touch_of_death
			Ability twin_snakes
			Ability fists_of_earth
			Ability arm_of_the_destroyer
			Ability demolish
			Ability fists_of_wind
			Ability steel_peak
			Ability mantra
			Ability howling_fist
			Ability perfect_balance
			Ability rockbreaker
			Ability shoulder_tackle
			Ability fists_of_fire
			Ability one_ilm_punch
			Ability dragon_kick
		}
		elseif(job.equals("WAR"){
			//================================================================================================== WAR
			Ability heavy_swing
			Ability foresight
			Ability skull_sunder
			Ability fracture
			Ability bloodbath
			Ability brutal_swing
			Ability overpower
			Ability tomahawk
			Ability maim
			Ability berserk
			Ability mercy_stroke
			Ability butchers_block
			Ability thrill_of_battle
			Ability storms_path
			Ability holmgang
			Ability vengeance
			Ability storms_eye
			Ability defiance
			Ability inner_beast
			Ability unchained
			Ability steel_cyclone
			Ability infuriate
		}
		elseif(job.equals("DRG"){
			//================================================================================================== DRG
			Ability true_thrust
			Ability feint
			Ability vorpal_thrust
			Ability keen_flurry
			Ability impulse_drive
			Ability leg_sweep
			Ability heavy_thrust
			Ability piercing_talon
			Ability life_surge
			Ability invigorate
			Ability full_thrust
			Ability phlebotomize
			Ability blood_for_blood
			Ability disembowel
			Ability doom_spike
			Ability ring_of_thorns
			Ability chaos_thrust
			Ability jump
			Ability elusive_jump
			Ability spineshatter_dive
			Ability power_surge
			Ability dragonfire_dive	
		}
		elseif(job.equals("BRD"){
			//================================================================================================== BRD
			Ability heavy_shot
			Ability straight_shot
			Ability raging_strikes
			Ability venomous_bite
			Ability miserys_end
			Ability shadowbind
			Ability bloodletter
			Ability repelling_shot
			Ability quick_nock
			Ability swiftsong
			Ability hawks_eye
			Ability windbite
			Ability quelling_strikes
			Ability barrage
			Ability blunt_arrow
			Ability flaming_arrow
			Ability wide_volley
			Ability mages_ballad
			Ability foe_requiem
			Ability armys_paeon
			Ability rain_of_death
			Ability battle_voice
		}
		elseif(job.equals("NIN"){
*/
			//================================================================================================== NIN
			ability_dictionary.put(110, new Ability(110, "Spinning Edge", 1, 100, 150, null, 0, 2.5, 60, false, -1, new Integer[]{112, 126}, GCD_LIST_INDEX, null, 100, false));
			ability_dictionary.put(111, new Ability(111, "Perfect Dodge", 2, 0, 0, null, 0, 120, 0, false, -1, null, -1, null, 100, false)); //TODO insert buff
			ability_dictionary.put(112, new Ability(112, "Gust Slash", 4, 100, 200, null, 0, 2.5, 50, false, 110, new Integer[]{121, 124}, GCD_LIST_INDEX, null, 100, false));
			ability_dictionary.put(113, new Ability(113, "Kiss of the Wasp", 6, 0, 0, null, 0, 5, 0, false, -1, null, KISS_LIST_INDEX, null, 100, true)); //TODO insert buff
			ability_dictionary.put(114, new Ability(114, "Mutilate", 8, 60, 60, null, 0, 2.5, 80, false, -1, null, GCD_LIST_INDEX, null, 100, false)); //TODO insert dot
			ability_dictionary.put(115, new Ability(115, "Hide", 10, 0, 0, null, 0, 20, 0, false, -1, null, -1, null, 100, false)); //Currently not programmed to unlock Trick Attack/Sneak Attack
			ability_dictionary.put(116, new Ability(116, "Assassinate", 12, 200, 200, null, 0, 40, 0, false, -1, null, -1, null, 20, false));
			ability_dictionary.put(117, new Ability(117, "Throwing Dagger", 15, 120, 120, null, 0, 2.5, 120, false, -1, null, GCD_LIST_INDEX, null, 100, false));
			ability_dictionary.put(118, new Ability(118, "Mug", 15, 140, 140, null, 0, 90, 0, false, -1, null, -1, null, 100, false));
			ability_dictionary.put(119, new Ability(119, "Goad", 18, 0, 0, null, 0, 180, 0, false, -1, null, -1, null, 100, false));
			ability_dictionary.put(120, new Ability(120, "Sneak Attack", 22, 500, 500, null, 0, 60, 0, false, 136, null, SATA_LIST_INDEX, null, 100, false));
			ability_dictionary.put(121, new Ability(121, "Aeolian Edge", 26, 100, 320, null, 0, 2.5, 60, false, 112, null, GCD_LIST_INDEX, null, 100, false));
			ability_dictionary.put(122, new Ability(122, "Kiss of the Viper", 30, 0, 0, null, 0, 5, 0, false, -1, null, KISS_LIST_INDEX, null, 100, true)); //TODO insert buff
			ability_dictionary.put(123, new Ability(123, "Jugulate", 34, 80, 80, null, 0, 30, 0, false, -1, null, -1, null, 100, false));
			ability_dictionary.put(124, new Ability(124, "Dancing Edge", 38, 100, 260, null, 0, 2.5, 50, false, 112, null, GCD_LIST_INDEX, null, 100, false)); //TODO insert buff
			ability_dictionary.put(125, new Ability(125, "Death Blossom", 42, 100, 100, null, 0, 2.5, 120, true, -1, null, GCD_LIST_INDEX, null, 100, false));
			ability_dictionary.put(126, new Ability(126, "Shadow Fang", 46, 100, 200, null, 0, 2.5, 70, false, 110, null, GCD_LIST_INDEX, null, 100, false)); //TODO insert dot
			ability_dictionary.put(127, new Ability(127, "Trick Attack", 50, 400, 400, null, 0, 60, 0, false, 136, null, SATA_LIST_INDEX, null, 100, false));//TODO insert buff
			ability_dictionary.put(128, new Ability(128, "Shukuchi", 40, 0, 0, null, 0, 60, 0, false, 0, null, -1, null, 100, false));
			ability_dictionary.put(129, new Ability(129, "Kassatsu", 50, 0, 0, null, 0, 120, 0, false, 0, null, -1, null, 100, false)); //TODO insert buff
			ability_dictionary.put(130, new Ability(130, "Fuma Shuriken", 30, 240, 240, null, 0.5, 20, 0, false, -1, null, NINJUTSU_LIST_INDEX, null, 100, false));
			ability_dictionary.put(131, new Ability(131, "Katon", 35, 180, 180, null, 1, 20, 0, true, -1, null, NINJUTSU_LIST_INDEX, null, 100, false));
			ability_dictionary.put(132, new Ability(132, "Raiton", 35, 360, 360, null, 1, 20, 0, false, -1, null, NINJUTSU_LIST_INDEX, null, 100, false));
			ability_dictionary.put(133, new Ability(133, "Hyoton", 45, 140, 140, null, 1, 20, 0, false, -1, null, NINJUTSU_LIST_INDEX, null, 100, false));
			ability_dictionary.put(134, new Ability(134, "Huton", 45, 0, 0, null, 1.5, 20, 0, false, -1, null, NINJUTSU_LIST_INDEX, null, 100, false)); //TODO insert buff
			ability_dictionary.put(135, new Ability(135, "Doton", 45, 0, 0, null, 1.5, 20, 0, true, -1, null, NINJUTSU_LIST_INDEX, null, 100, false)); //TODO insert dot
			ability_dictionary.put(136, new Ability(136, "Suiton", 45, 180, 180, null, 1.5, 20, 0, false, -1, new Integer[]{120, 127}, NINJUTSU_LIST_INDEX, null, 100, false)); //TODO insert buff
/*
		}
		elseif(job.equals("WHM"){
			//================================================================================================== WHM
			Ability stone
			Ability cure
			Ability aero
			Ability cleric_stance
			Ability protect
			Ability medica
			Ability raise
			Ability fluid_aura
			Ability esuna
			Ability stone_ii
			Ability repose
			Ability cure_ii
			Ability stoneskin
			Ability shroud_of_saints
			Ability cure_iii
			Ability aero_ii
			Ability medica_ii
			Ability stoneskin_ii
			Ability presence_of_mind
			Ability regen
			Ability divine_seal
			Ability holy
			Ability benediction
		}
		elseif(job.equals("BLM"){
			//================================================================================================== BLM
			Ability blizzard
			Ability fire
			Ability transpose
			Ability thunder
			Ability surecast
			Ability sleep
			Ability blizzard_ii
			Ability scathe
			Ability fire_ii
			Ability thunder_ii
			Ability swiftcast
			Ability manaward
			Ability fire_iii
			Ability blizzard_iii
			Ability lethargy
			Ability thunder_iii
			Ability aetherial_manipulation
			Ability convert
			Ability freeze
			Ability apocatastasis
			Ability manawall
			Ability flare
		}
		elseif(job.equals("SCH") || job.equals("SMN")){
			//================================================================================================== SCH/SMN
			Ability ruin
			Ability bio
			Ability summon
			Ability physick
			Ability aetherflow
			Ability energy_drain
			Ability miasma
			Ability virus
			Ability summon_ii
			Ability sustain
			Ability resurrection
			Ability bio_ii
			Ability bane
			Ability eye_for_an_eye
			Ability ruin_ii
			Ability rouse
			Ability miasma_ii
			Ability shadow_flare
			if(job.equals("SCH"){
				//================================================================================================== SCH
				Ability adloquium
				Ability succor
				Ability leeches
				Ability sacred_soil
				Ability lustrate
			}
			elseif(job.equals("SMN"){
				//================================================================================================== SMN
				Ability summon_iii
				Ability fester
				Ability tri_disaster
				Ability spur
				Ability enkindle
			}
		}
		//================================================================================================== Cross-Class
						
			/*
				int index, String name, int level, int base_potency, int combo_potency, Dot dot_name,double base_cast_time, 
				double base_recast_time, int tp_cost, boolean aoe, int ability_combo_unlocked_by, Integer[] combo_unlocks,
				int cooldown_list_index, Buff buff_name, double only_available_under_hp_percent
			
	if(job.equals("WAR"){
		Ability savage_blade_x = new Ability(210, );
		Ability flash_x = new Ability(211, );
		Ability convalescence_x = new Ability(212, );
		Ability provoke_x = new Ability(213, );
		Ability awareness_x = new Ability(214, );
	}
	*/
	if(job.equals("WAR") || job.equals("DRG") || job.equals("BRD") || job.equals("NIN")){
		ability_dictionary.put(215, new Ability(215, "Featherfoot", 4, 0, 0, null, 0, 90, 0, false, -1, null, -1, null, 100, false));
		ability_dictionary.put(216, new Ability(216, "Second Wind", 8, 0, 0, null, 0, 120, 0, false, -1, null, -1, null, 100, false));
		ability_dictionary.put(217, new Ability(217, "Haymaker", 10, 170, 170, null, 0, 2.5, 40, false, 9999, null, GCD_LIST_INDEX, null, 100, false)); //Not currently programmed to work
		ability_dictionary.put(218, new Ability(218, "Internal Release", 12, 0, 0, null, 0, 60, 0, false, -1, null, -1, null, 100, false));
		ability_dictionary.put(219, new Ability(219, "Mantra", 42, 0, 0, null, 0, 120, 0, false, -1, null, -1, null, 100, false));
	}
	/*if(job.equals("PLD") || job.equals("MNK") || job.equals("DRG")){		
		Ability foresight_x = new Ability(220, );
		Ability skull_sunder_x = new Ability(221, );
		Ability fracture_x = new Ability(222, );
		Ability bloodbath_x = new Ability(223, );
		Ability mercy_stroke_x = new Ability(224, );
	}*/
	if(job.equals("MNK") || job.equals("BRD") ||job.equals("NIN")){
		ability_dictionary.put(225, new Ability(225, "Feint", 2, 120, 120, null, 0, 2.5, 80, false, -1, null, GCD_LIST_INDEX, null, 100, false));
		ability_dictionary.put(226, new Ability(226, "Keen Flurry", 6, 0, 0, null, 0, 90, 0, false, -1, null, -1, null, 100, false));
		ability_dictionary.put(227, new Ability(227, "Invigorate", 22, 0, 0, null, 0, 120, 0, false, -1, null, -1, null, 100, false)); //TODO insert buff
		ability_dictionary.put(228, new Ability(228, "Blood for Blood", 34, 0, 0, null, 0, 80, 0, false, -1, null, -1, null, 100, false)); //TODO insert buff
	}
	/*
	if(job.equals("SCH")){
		Ability cure_x
		Ability aero_x
		Ability cleric_stance_x
		Ability protect_x
		Ability raise_x
		Ability stoneskin_x
	}
	if(job.equals("WHM") || job.equals("SCH") || job.equals("SMN")){
		Ability surecast_x
		Ability blizzard_ii_x
		Ability swiftcast_x
	}
	if(job.equals("WHM") || job.equals("BLM")){
		Ability ruin_x
		Ability physick_x
		Ability virus_x
		Ability eye_for_an_eye_x
	}
*/	
	//================================================================================================= Add abilities to cd list matrix
		for(Map.Entry<Integer, Ability> entry : ability_dictionary.entrySet()){
			if(entry.getValue().cooldown_list_index >= 0){
				cooldown_matrix.get(entry.getValue().cooldown_list_index).add(entry.getKey());
			}
		}
	}
}
