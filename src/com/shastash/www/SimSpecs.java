package com.shastash.www;

public class SimSpecs {
	
	private String job;
	
	private double weap_dmg = 0;
	private double weap_aa = 0;
	private double weap_delay = 0;
	private double main_stat = 0;
	private double crit_stat = 0;
	private double det_stat = 0;
	private double speed_stat = 0;
	
	private boolean party_buff_checked = false;
	private boolean selene_buff_checked = false;
	private boolean teammate_slashing_debuff_checked = false;
	
	private boolean display_summary_checked = true;
	private boolean display_dps_chart_checked = true;
	private boolean display_tp_chart_checked = true;
	private boolean display_ability_usage_checked = true;
	
	private double sim_duration = 0;
	
	public SimSpecs(String job, double weap_dmg, double weap_aa, double weap_delay, 
			double main_stat, double crit_stat, double det_stat, double speed_stat,	
			boolean party_buff_checked, boolean selene_buff_checked, boolean teammate_slashing_debuff_checked, 
			boolean display_summary_checked, boolean display_dps_chart_checked, boolean display_tp_chart_checked, 
			boolean display_ability_usage_checked, double sim_duration){
		this.job = job;
		this.weap_dmg = weap_dmg;
		this.weap_aa = weap_aa;
		this.weap_delay = weap_delay;
		this.main_stat = main_stat;
		this.crit_stat = crit_stat;
		this.det_stat = det_stat;
		this.speed_stat = speed_stat;
		this.party_buff_checked = party_buff_checked;
		this.selene_buff_checked = selene_buff_checked;
		this.teammate_slashing_debuff_checked = teammate_slashing_debuff_checked;
		this.display_summary_checked = display_summary_checked;
		this.display_dps_chart_checked = display_dps_chart_checked;
		this.display_tp_chart_checked = display_tp_chart_checked;
		this.display_ability_usage_checked = display_ability_usage_checked;
		this.sim_duration = sim_duration;
	}
	
	public String getJob(){
		return job;
	}
	public double getWeapDmg(){
		return weap_dmg;
	}
	public double getWeapAa(){
		return weap_aa;
	}
	public double getWeapDelay(){
		return weap_delay;
	}
	public double getMainStat(){
		return main_stat;
	}
	public double getCritStat(){
		return crit_stat;
	}
	public double getDetStat(){
		return det_stat;
	}
	public double getSpeedStat(){
		return speed_stat;
	}
	public boolean getPartyBuffChecked(){
		return party_buff_checked;
	}
	public boolean getSeleneBuffChecked(){
		return selene_buff_checked;
	}
	public boolean getTeammateSlashingDebuffChecked(){
		return teammate_slashing_debuff_checked;
	}
	public boolean getDisplaySummaryChecked(){
		return display_summary_checked;
	}
	public boolean getDisplayDpsChartChecked(){
		return display_dps_chart_checked;
	}
	public boolean getDisplayTpChartChecked(){
		return display_tp_chart_checked;
	}
	public boolean getDisplayAbilityUsageChecked(){
		return display_ability_usage_checked;
	}
	public double getDuration(){
		return sim_duration;
	}
}
