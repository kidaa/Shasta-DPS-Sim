package com.shastash.www;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class Main implements ActionListener{
	private String TITLE = "Shasta's FFXIV DPS Simulator";
	protected static JFrame frame;
	
	public Main(){
		initUI();
	}
	
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				Main ex = new Main();
			}
		});
	}
	
	private void initUI(){
		frame = new JFrame(TITLE);
		String main_stat = "DEX";
		JLabel label;
		JComboBox combo_box, job_box;
		JCheckBox check_box;
		JTextField display_field;
		HintTextField hint_text_field;
		JButton button;
		GridBagConstraints c;
		String[] job_strings = {"- - - ", "NIN"};
		String[] nin_gcd_strings = {"GCDs", "Aeolian Edge", "Dancing Edge", "Death Blossom", 
				"Gust Slash", "Mutilate", "Shadow Fang", "Spinning Edge", "Throwing Dagger"};		//Global Cooldown Abilities
		String[] nin_nongcd_strings = {"Non-GCDs", "Assassinate", "Jugulate",
				"Mug", "Sneak Attack", "Trick Attack"};												//Non-Global Cooldown Abilities
		String[] nin_buff_strings = {"Buffs", "Blood-for-Blood", "Internal Release", 
				"Invigorate", "Kassatsu", "Kiss of the Viper", "Kiss of the Wasp"};					//Buffs
		String[] nin_ninjutsu_strings = {"Ninjutsu", "Doton", "Fuma Shuriken", 
				"Huton", "Hyoton", "Katon", "Raiton", "Suiton"};									//Ninjutsu
		
		nin_gcd_strings[0] = adjustDropdownItemWidth(nin_gcd_strings);
		nin_nongcd_strings[0] = adjustDropdownItemWidth(nin_nongcd_strings);
		nin_buff_strings[0] = adjustDropdownItemWidth(nin_buff_strings);
		nin_ninjutsu_strings[0] = adjustDropdownItemWidth(nin_ninjutsu_strings);
		
		//Use CardLayout for the input area based on the job selection box
		
		final JPanel input_pane = new JPanel(new GridBagLayout());
		frame.add(input_pane);
		
		//=============================================================================================== Line 1: Job Select
		c = new GridBagConstraints();
		label = new JLabel("Select Job");
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		job_box = new JComboBox(job_strings);
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		//c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(job_box, c);

		//=============================================================================================== Line 2: Input Gear Stats
		c = new GridBagConstraints();
		label = new JLabel("Input Gear Stats");
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("W Dmg", 6);
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(hint_text_field, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("W AA", 6);
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 2;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(hint_text_field, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("W Delay", 6);
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 3;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(hint_text_field, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField(main_stat, 6);
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 4;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(hint_text_field, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("Crit", 6);
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 5;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(hint_text_field, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("Det", 6);
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 6;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(hint_text_field, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("Speed", 6);
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 7;
		c.gridy = 1;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(hint_text_field, c);
		
		//=============================================================================================== Line 3: Damage Formula
		c = new GridBagConstraints();
		label = new JLabel("Ability Damage Formula");
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("(WD*.2714745 + "+main_stat+"*.1006032 + (DET-202)*.0241327 + WD*"+main_stat+"*.0036167 + WD*(DET-202)*.0010800 - 1) * (Potency / 100)");
		hint_text_field.setEditable(false);
		hint_text_field.setFocusable(false);
		hint_text_field.setForeground(Color.BLACK);
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 8;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(hint_text_field, c);
		
		c = new GridBagConstraints();
		label = new JLabel("Auto-Attack Damage Formula");
		c.gridx = 0;
		c.gridy = 3;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("(WD*.2714745 + DEX*.1006032 + (DET-202)*.0241327 + WD*DEX*.0036167 + WD*(DET-202)*.0022597 - 1) * (W Delay / 3)");
		hint_text_field.setEditable(false);
		hint_text_field.setFocusable(false);
		hint_text_field.setForeground(Color.BLACK);
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 8;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(hint_text_field, c);

		//=============================================================================================== Lines 4 & 5: Input Rotation
		c = new GridBagConstraints();
		label = new JLabel("Input Rotation");
		c.gridx = 0;
		c.gridy = 4;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		combo_box = new JComboBox(nin_gcd_strings);
		c.gridx = 1;
		c.gridy = 4;
		c.gridwidth = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(combo_box, c);
		
		c = new GridBagConstraints();
		combo_box = new JComboBox(nin_nongcd_strings);
		c.gridx = 3;
		c.gridy = 4;
		c.gridwidth = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(combo_box, c);
		
		c = new GridBagConstraints();
		combo_box = new JComboBox(nin_buff_strings);
		c.gridx = 1;
		c.gridy = 5;
		c.gridwidth = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(combo_box, c);
		
		c = new GridBagConstraints();
		combo_box = new JComboBox(nin_ninjutsu_strings);
		c.gridx = 3;
		c.gridy = 5;
		c.gridwidth = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(combo_box, c);
		
		c = new GridBagConstraints();
		button = new JButton("Add >>");
		c.gridx = 5;
		c.gridy = 4;
		c.gridwidth = 2;
		c.gridheight = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(button, c);
		
		c = new GridBagConstraints();
		button = new JButton("Add as Pre-Cast Ability");
		c.gridx = 6;
		c.gridy = 4;
		c.gridwidth = 2;
		c.gridheight = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(button, c);
		
		//=============================================================================================== Line 6: Party Buffs
		c = new GridBagConstraints();
		label = new JLabel("Party Buffs");
		c.gridx = 0;
		c.gridy = 6;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();																//Checkbox for 3% party buff
		check_box = new JCheckBox("<html><div style='text-align:center'>In Party<br/>(3% "
				+main_stat+" Buff)</div></html>", false);
		c.gridx = 1;
		c.gridy = 6;
		c.gridwidth = 2;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(check_box, c);
		
		c = new GridBagConstraints();																//Checkbox for Selene Buff
		check_box = new JCheckBox("<html><div style='text-align:center'>Selene "
				+ "<br/>Speed Buff</div></html>", false);
		c.gridx = 2;
		c.gridy = 6;
		c.gridwidth = 2;
		c.insets = new Insets(5, 30, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(check_box, c);
		
		c = new GridBagConstraints();																//Checkbox for Slashing Debuff
		check_box = new JCheckBox("<html><div style='text-align:center'>10% Slashing Debuff "
				+ "<br/>Applied By Teammate</div></html>", false);
		c.gridx = 3;
		c.gridy = 6;
		c.gridwidth = 3;
		c.insets = new Insets(5, 40, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(check_box, c);

		//=============================================================================================== Line 7: Simulation Specifics
		c = new GridBagConstraints();
		label = new JLabel("Simulation Specifics");
		c.gridx = 0;
		c.gridy = 7;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		hint_text_field = new HintTextField("Duration");
		hint_text_field.setHorizontalAlignment(JTextField.CENTER);
		c.gridx = 1;
		c.gridy = 7;
		c.insets = new Insets(5, 10, 5, 0); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(hint_text_field, c);c = new GridBagConstraints();
		
		label = new JLabel("secs");
		c.gridx = GridBagConstraints.RELATIVE;
		c.gridy = 7;
		c.insets = new Insets(5, 2, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();																//Checkbox for Summary Display
		check_box = new JCheckBox("<html><div style='text-align:center'>Display"
				+ "<br/>Summary</div></html>", true);
		c.gridx = 3;
		c.gridy = 7;
		c.insets = new Insets(5, 5, 5, 5); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(check_box, c);
		
		c = new GridBagConstraints();																//Checkbox for DPS Chart Display
		check_box = new JCheckBox("<html><div style='text-align:center'>Display <br/>"
				+ "DPS Chart</div></html>", true);
		c.gridx = 4;
		c.gridy = 7;
		c.insets = new Insets(5, 5, 5, 5); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(check_box, c);
		
		c = new GridBagConstraints();																//Checkbox for TP Chart Display
		check_box = new JCheckBox("<html><div style='text-align:center'>Display <br/>"
				+ "TP Chart</div></html>", true);
		c.gridx = 5;
		c.gridy = 7;
		c.insets = new Insets(5, 5, 5, 5); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(check_box, c);
		
		c = new GridBagConstraints();																//Checkbox for Ability Breakdown Display
		check_box = new JCheckBox("<html><div style='text-align:center'>Display <br/>"
				+ "Ability Usage</div></html>", true);
		c.gridx = 6;
		c.gridy = 7;
		c.gridwidth = 2;
		c.insets = new Insets(5, 5, 5, 5); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_START;
		c.ipadx = 3;
		c.ipady = 1;
		input_pane.add(check_box, c);
		
		//=============================================================================================== Line 8: Results
		JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 8;
		c.gridwidth = 9;
		c.insets = new Insets(0,5,0,5); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		input_pane.add(separator, c);
		
		c = new GridBagConstraints();
		label = new JLabel("<html><div style='text-align:center; font-size:130%'>Results</div></html");
		c.gridx = 0;
		c.gridy = 9;
		c.gridwidth = 8;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.CENTER;
		input_pane.add(label, c);
		
		//new panel here, probably BoxLayout
		
		
		c = new GridBagConstraints();
		label = new JLabel("Average DPS");
		c.gridx = 0;
		c.gridy = 10;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		display_field = new JTextField();
		display_field.setEditable(false);
		display_field.setFocusable(false);
		display_field.setForeground(Color.BLACK);
		c.gridx = 1;
		c.gridy = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(display_field, c);
		
		c = new GridBagConstraints();
		label = new JLabel("Total Damage");
		c.gridx = 0;
		c.gridy = 11;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(label, c);
		
		c = new GridBagConstraints();
		display_field = new JTextField();
		display_field.setEditable(false);
		display_field.setFocusable(false);
		display_field.setForeground(Color.BLACK);
		c.gridx = 1;
		c.gridy = 11;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(5, 10, 5, 10); //top, left, bottom, right external padding
		c.anchor = GridBagConstraints.LINE_END;
		input_pane.add(display_field, c);
		
		//==============================================================================================Configure window behavior
		frame.setSize(1024, 768);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	//Inserts hyphens to the front and back of the dropdown menu headers
	//Formatting for appearance purposes only
	private String adjustDropdownItemWidth(String[] list){
		String longest_string = "";
		String hyphens = "";
		int diff_count = 0;
		for(String s: list){											//Find longest string in the list
			if(s.length() > longest_string.length()){
				longest_string = s;
			}
		}
		diff_count = longest_string.length()-list[0].length();			//Find diff between header & longest string lengths
		if(diff_count % 2 > 0){											//If difference is odd, add one
			diff_count++;
		}
		for(int i = 0; i < diff_count/2; i++){
			hyphens += "- ";
		}
		return hyphens + list[0] + " "+ hyphens;
	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}