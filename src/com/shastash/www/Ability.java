package com.shastash.www;

import java.util.ArrayList;

public class Ability implements Cloneable{
	int index;
	String name;
	int level;
	int base_potency;
	int combo_potency;
	Dot dot;
	double base_cast_time; //In seconds
	double base_recast_time; //AKA cooldown time in seconds
	int tp_cost;
	boolean aoe;
	int ability_combo_unlocked_by; //Pre-requisite ability required, reference by index
	ArrayList<Integer> abilities_combo_unlocks;
	int cooldown_list_index;
	Buff buff;
	double only_available_under_hp_percent = 100;
	boolean eligible_for_precast; //Can this be put on before starting simulator? e.g. permanent buffs
	
	public Ability(int index, String name, int level, int base_potency, int combo_potency, Dot dot,double base_cast_time, 
			double base_recast_time, int tp_cost, boolean aoe, int ability_combo_unlocked_by, Integer[] combo_unlocks,
			int cooldown_list_index, Buff buff, double only_available_under_hp_percent, boolean eligible_for_precast){
		this.index = index;
		this.name = name;
		this.level = level;
		this.base_potency = base_potency;
		this.combo_potency = combo_potency;
		this.dot = dot;
		this.base_cast_time = base_cast_time;
		this.base_recast_time = base_recast_time;
		this.tp_cost = tp_cost;
		this.aoe = aoe;
		this.ability_combo_unlocked_by = ability_combo_unlocked_by;
		abilities_combo_unlocks = new ArrayList<Integer>();
		for(Integer i: combo_unlocks){
			this.abilities_combo_unlocks.add(i);
		}
		this.cooldown_list_index = cooldown_list_index;
		this.buff = buff;
		this.only_available_under_hp_percent = only_available_under_hp_percent;
		this.eligible_for_precast = eligible_for_precast;
	}
}
