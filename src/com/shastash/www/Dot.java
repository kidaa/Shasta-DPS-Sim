package com.shastash.www;

public class Dot {
	int index;
	String dot_name;
	double dot_duration;
	int damage_per_tick;
	
	public Dot(int index, String dot_name, double dot_duration, int damage_per_tick){
		this.index = index;
		this.dot_name = dot_name;
		this.dot_duration = dot_duration;
		this.damage_per_tick = damage_per_tick;
	}
}
