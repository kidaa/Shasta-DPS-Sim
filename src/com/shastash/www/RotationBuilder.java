package com.shastash.www;

import java.util.ArrayList;

public class RotationBuilder{
	ArrayList<Ability> non_gcd_list; //list for handling non-gcd abilities on cooldown
	ArrayList<Ability> gcd_list; //list for handling gcd abilities on cooldown
	ArrayList<Ability> available_gcd_abilities; //list for handling available gcd abilities
	ArrayList<Ability> available_non_gcd_abilities; //list for handling available non-gcd abilities
	ArrayList<Dot> applied_dots; //list for handling applied dots on target
	ArrayList<Buff> applied_buffs; //list for buff methods
	
	public RotationBuilder(SimSpecs sim_specs){
		non_gcd_list = new ArrayList<Ability>();
		gcd_list = new ArrayList<Ability>();
		available_gcd_abilities = new ArrayList<Ability>();
		available_non_gcd_abilities = new ArrayList<Ability>();
		applied_dots = new ArrayList<Dot>();
		applied_buffs = new ArrayList<Buff>();
	}
}
