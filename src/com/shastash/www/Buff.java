package com.shastash.www;

public class Buff {
	int index;
	String buff_name;
	double buff_duration;
	
	public Buff(int index, String buff_name, double buff_duration){
		this.index = index;
		this.buff_name = buff_name;
		this.buff_duration = buff_duration;
	}
}
